import org.apache.commons.csv.CSVRecord;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import storage.Storage;
import storage.subjecthierarchy.utils.CsvUtils;
import storage.subjecthierarchy.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class StorageTests {

    private static final String resourceFileName = "data.csv";
    private Storage storage;

    @BeforeTest
    public void setup() {
        storage = new Storage(resourceFileName);
    }

    @Test
    public void storageLowestTest() {
        String lowestElementsFileName = "lowest_elements.csv";

        storage.writeLowestHierarchyItemsToFile(lowestElementsFileName);

        List<CSVRecord> csvRecords = CsvUtils.readFromFile(new File(FileUtils.getTargetFilePath(lowestElementsFileName)));
        assertThat(csvRecords).as("Lowest element rows should have one string").hasSize(1);

        List<String> values = new ArrayList<>();
        csvRecords.get(0).iterator().forEachRemaining(values::add);
        assertThat(values)
                .as("Lowest elements file string")
                .containsOnly(
                        "Румянцево",
                        "Говорово",
                        "Левобережный р-н",
                        "Румянцево",
                        "Здравница",
                        "Ворогово");
    }

    @Test()
    public void anagramsTest() {
        String anagramsFileName = "anagrams.csv";

        Storage storage = new Storage(resourceFileName);
        storage.writeAnagramsToFile(anagramsFileName);

        List<CSVRecord> csvRecords = CsvUtils.readFromFile(new File(FileUtils.getTargetFilePath(anagramsFileName)));
        assertThat(csvRecords).as("Anagram file rows should have one string").hasSize(2);

        List<String> values = new ArrayList<>();
        csvRecords.forEach(record -> values.add(String.join(",", record)));
        assertThat(values)
                .as("Anagram file strings")
                .containsOnly(
                        "Румянцево,Румянцево",
                        "Говорово,Ворогово");
    }
}
