package storage;

public class CsvLine {

    private String name;
    private String parent;

    public CsvLine(String name, String parent) {
        this.name = name;
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public String getParent() {
        return parent;
    }
}
