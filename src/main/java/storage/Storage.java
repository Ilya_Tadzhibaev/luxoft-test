package storage;

import storage.subjecthierarchy.SubjectHierarchyManager;
import storage.subjecthierarchy.enums.SubjectNodeCsvColumn;
import storage.subjecthierarchy.utils.CsvUtils;
import storage.subjecthierarchy.utils.FileUtils;
import storage.subjecthierarchy.utils.SubjectUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Storage {

    List<SubjectNode> subjectNodes;
    private String resourceFileName;

    public Storage(String resourceFileName) {
        this.resourceFileName = resourceFileName;
        getSubjectNodesFromFile();
    }

    private void getSubjectNodesFromFile() {
        subjectNodes = SubjectUtils.makeSubjectNodes(
                CsvUtils.getCsvRecords(resourceFileName, SubjectNodeCsvColumn.values()));
    }

    public void writeLowestHierarchyItemsToFile(String fileName) {
        List<SubjectNode> lowestSubjects =
                SubjectUtils.getLowestSubjects(new SubjectHierarchyManager(new ArrayList<>(subjectNodes)).createHierarchy());
        CsvUtils.writeToFile(new File(FileUtils.getTargetFilePath(fileName)), lowestSubjects.stream().map(SubjectNode::getName).collect(Collectors.joining(",")));
    }

    public void writeAnagramsToFile(String fileName) {
        List<List<String>> anagrams = SubjectUtils.getAnagrams(subjectNodes.stream().map(SubjectNode::getName).collect(Collectors.toList()));
        String finalString = anagrams.stream().map(list -> String.join(",", list)).collect(Collectors.joining(System.lineSeparator()));
        CsvUtils.writeToFile(new File(FileUtils.getTargetFilePath(fileName)), finalString);
    }

}
