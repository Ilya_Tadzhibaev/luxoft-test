package storage;

import java.util.ArrayList;
import java.util.List;

public class SubjectNode {

    private String name;
    private String parentName;
    private SubjectNode parent;
    private List<SubjectNode> children;

    public SubjectNode(String name, String parentName) {
        this.name = name;
        this.parentName = parentName;
        children = new ArrayList<>();
    }

    public void addChild(SubjectNode child) {
        children.add(child);
    }

    public String getName() {
        return name;
    }

    public SubjectNode getParent() {
        return parent;
    }

    public SubjectNode setParent(SubjectNode parent) {
        this.parent = parent;
        return this;
    }

    public String getParentName() {
        return parentName;
    }

    public List<SubjectNode> getChildren() {
        return children;
    }
}
