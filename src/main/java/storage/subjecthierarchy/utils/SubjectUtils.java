package storage.subjecthierarchy.utils;

import org.apache.commons.csv.CSVRecord;
import storage.SubjectNode;
import storage.subjecthierarchy.SubjectsHierarchy;
import storage.subjecthierarchy.enums.SubjectNodeCsvColumn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SubjectUtils {

    private SubjectUtils() {
        throw new RuntimeException("Utility class");
    }

    public static List<SubjectNode> makeSubjectNodes(List<CSVRecord> csvRecords) {
        return csvRecords.stream()
                .map(record -> new SubjectNode(
                        record.get(SubjectNodeCsvColumn.NAME.getColumnName()),
                        record.get(SubjectNodeCsvColumn.PARENT.getColumnName())))
                .collect(Collectors.toList());
    }

    public static SubjectNode extractMainSubject(List<SubjectNode> subjectNodes) {
        return subjectNodes.stream()
                .filter(subject -> subject.getParentName().equals(""))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Main subject not found"));
    }

    public static List<SubjectNode> getLowestSubjects(SubjectsHierarchy subjectsHierarchy) {
        return extractLowestSubject(new ArrayList<>(), subjectsHierarchy.getMainSubject());
    }

    private static List<SubjectNode> extractLowestSubject(List<SubjectNode> subjectNodes, SubjectNode subjectNode) {
        if (subjectNode.getChildren().isEmpty()) {
            subjectNodes.add(subjectNode);
            return subjectNodes;
        }
        for (SubjectNode child : subjectNode.getChildren()) {
            extractLowestSubject(subjectNodes, child);
        }
        return subjectNodes;
    }

    public static List<List<String>> getAnagrams(List<String> names) {
        List<List<String>> anagramsList = new ArrayList<>();
        for (int i = 0; i < names.size(); i++) {
            for (int j = i; j < names.size() - 1; j++) {
                String name = names.get(i);
                String nextName = names.get(j + 1);
                if (name.length() == nextName.length()) {
                    char[] nameChars = name.toLowerCase().toCharArray();
                    char[] nextNameChars = nextName.toLowerCase().toCharArray();
                    Arrays.sort(nameChars);
                    Arrays.sort(nextNameChars);
                    if (Arrays.equals(nameChars, nextNameChars)) {
                        anagramsList.add(Arrays.asList(name, nextName));
                    }
                }
            }
        }
        return anagramsList;
    }
}
