package storage.subjecthierarchy.utils;

import java.io.File;

public class FileUtils {

    public FileUtils() {
        throw new RuntimeException("Utility class");
    }

    public static String getTargetFilePath(String fileName) {
        return System.getProperty("user.dir") +
                File.separator +
                "target" +
                File.separator +
                fileName;
    }
}
