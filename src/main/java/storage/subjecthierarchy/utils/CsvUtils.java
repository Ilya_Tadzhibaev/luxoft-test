package storage.subjecthierarchy.utils;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import storage.subjecthierarchy.enums.CsvColumn;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class CsvUtils {

    private CsvUtils() {
        throw new RuntimeException("Utility class");
    }

    public static List<CSVRecord> getCsvRecords(String resourceFileName, CsvColumn[] csvColumns) {
        List<CSVRecord> csvRecords = new ArrayList<>();
        try (Reader in = new FileReader(Objects.requireNonNull(CsvUtils.class.getClassLoader().getResource(resourceFileName)).getFile())) {
            csvRecords = CSVFormat.DEFAULT
                    .withHeader(Stream.of(csvColumns).map(CsvColumn::getColumnName).toArray(String[]::new))
                    .withFirstRecordAsHeader()
                    .parse(in)
                    .getRecords();
        } catch (IOException e) {
            e.printStackTrace(); // should be logged in a more pretty way
        }
        return csvRecords;
    }

    public static void writeToFile(File outputFile, String outputString) {
        try (Writer out = new FileWriter(outputFile)) {
            out.write(outputString);
        } catch (IOException e) {
            e.printStackTrace(); // should be logged in a more pretty way
        }
    }

    public static List<CSVRecord> readFromFile(File file) {
        List<CSVRecord> csvRecords = new ArrayList<>();
        try (Reader in = new FileReader(file)) {
            csvRecords = CSVFormat.DEFAULT
                    .parse(in)
                    .getRecords();
        } catch (IOException e) {
            e.printStackTrace(); // should be logged in a more pretty way
        }
        return csvRecords;
    }
}
