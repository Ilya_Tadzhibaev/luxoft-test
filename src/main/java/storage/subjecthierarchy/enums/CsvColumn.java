package storage.subjecthierarchy.enums;

public interface CsvColumn {

    String getColumnName();
}
