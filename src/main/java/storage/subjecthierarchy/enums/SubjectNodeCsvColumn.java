package storage.subjecthierarchy.enums;

public enum SubjectNodeCsvColumn implements CsvColumn {

    NAME("Name"),
    PARENT("Parent");

    private String columnName;

    SubjectNodeCsvColumn(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }
}
