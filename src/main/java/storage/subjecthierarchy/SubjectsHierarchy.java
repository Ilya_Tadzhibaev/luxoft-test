package storage.subjecthierarchy;

import storage.SubjectNode;

public class SubjectsHierarchy {

    private SubjectNode mainSubject;

    public boolean addSubjectsToHierarchy(SubjectNode subjectNodeToAdd) {
        return addSubject(mainSubject, subjectNodeToAdd);
    }

    private boolean addSubject(SubjectNode rootNode, SubjectNode subjectNodeToAdd) {
        if (rootNode.getName().equals(subjectNodeToAdd.getParentName())) {
            rootNode.addChild(subjectNodeToAdd);
            subjectNodeToAdd.setParent(rootNode);
            return true;
        } else {
            for (SubjectNode child : rootNode.getChildren()) {
                if (addSubject(child, subjectNodeToAdd)) {
                    return true;
                }
            }
        }
        return false;
    }

    public SubjectNode getMainSubject() {
        return mainSubject;
    }

    SubjectsHierarchy setMainSubject(SubjectNode mainSubject) {
        this.mainSubject = mainSubject;
        return this;
    }
}
