package storage.subjecthierarchy;

import storage.SubjectNode;
import storage.subjecthierarchy.utils.SubjectUtils;

import java.util.List;

public class SubjectHierarchyManager {

    private List<SubjectNode> subjectNodes;
    private SubjectsHierarchy subjectsHierarchy;


    public SubjectHierarchyManager(List<SubjectNode> subjectNodes) {
        this.subjectNodes = subjectNodes;
        this.subjectsHierarchy = new SubjectsHierarchy();
    }

    public SubjectsHierarchy createHierarchy() {
        SubjectNode mainSubject = SubjectUtils.extractMainSubject(subjectNodes);
        subjectNodes.remove(mainSubject);
        subjectsHierarchy.setMainSubject(mainSubject);

//        known issue: might fall into endless loop if can't find parent for element.
        while (!subjectNodes.isEmpty()) {
            for (int i = 0; i < subjectNodes.size(); i++) {
                if (subjectsHierarchy.addSubjectsToHierarchy(subjectNodes.get(i))) {
                    subjectNodes.remove(i);
                }
            }
        }
        return subjectsHierarchy;
    }
}

